# Cypress and Micro Frontends demo in GitLab

[![pipeline status](https://gitlab.com/davidggevorgyan/cypress-microfrontend/badges/master/pipeline.svg)](https://gitlab.com/davidggevorgyan/cypress-microfrontend/commits/master)

To start UI > `npx cypress open`

To run tests > `npx cypress run`

To get help > `npx cypress help`

To run one file > `npx cypress run --spec cypress/integration/todomvc-actions.spec.js`

To run tests via npm > `npm test` after editing package json test section to `"test": "cypress run"`

To setup eyes > `npx eyes-setup`

## Useful links

[Feed me. The site with micro frontends under the test](https://demo.microfrontends.com)

[Micro Frontends intro](https://martinfowler.com/articles/micro-frontends.html)

[Test a React Todo App Free by Andy van Slaars](https://docs.cypress.io/examples/examples/tutorials.html#Test-a-React-Todo-App)

[Introduction to Cypress Free by Gil Tayar](https://testautomationu.applitools.com/cypress-tutorial/)

[You don't know JS book](https://github.com/getify/You-Dont-Know-JS)

[Cypress site](https://www.cypress.io/)

[Cypress documentation](https://docs.cypress.io/)

[Cypress API reference to all Cypress commands and more](https://docs.cypress.io/api/api/table-of-contents.html)

[Cypress CLI documentation](https://docs.cypress.io/guides/guides/command-line.html)

[Mocha Tutorial on Test Automation University](https://testautomationu.applitools.com/mocha-javascript-tests/)

[Mocha: Cypress’ test runner](https://mochajs.org/)

[Applitools Visual Grid](https://applitools.com/visualgrid)

[Applitools Cypress Tutorial](https://applitools.com/tutorials/cypress.html)

[EditorConfig helps maintain consistent coding styles](https://editorconfig.org)

[An ESLint plugin for your Cypress tests](https://github.com/cypress-io/eslint-plugin-cypress)

[REST API testing with Cypress](https://www.merixstudio.com/blog/rest-api-testing-cypress/)
