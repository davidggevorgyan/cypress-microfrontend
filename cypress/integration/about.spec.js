import * as aboutPage from '../page-objects/about';

describe('About', () => {
  before(() => aboutPage.navigate());

  context('API', () => {
    it('About GET', () => {
      cy.request('GET', '/about').as('about');
      cy.get('@about').should((response) => {
        expect(response.status).to.eq(200);
      });
    });
  });

  context('E2E', () => {
    it('Section Render', () => {
      aboutPage.validateAboutHeader('About this site');
      aboutPage.validateAboutText('Cam wrote for martinfowler.com.', 3);
      cy.applitoolsSetup('About Page', 'About');
    });
  });
});
