import * as navigationPage from '../page-objects/navigation';

describe('Navigation', () => {
  context('E2E', () => {
    it('Check Navigation Calls', () => {
      const urls = [
        { tabName: 'Browse restaurants', elementLocator: '#Browse-container button' },
        { tabName: 'About', elementLocator: '#about' },
        { tabName: 'Surprise me', elementLocator: '#Restaurant-container button' }
      ];
      navigationPage.navigate(urls);
      cy.applitoolsSetup('Navigation Section', 'Navigation');
      navigationPage.validateHeader();
    });
  });
});
