import * as restaurantPage from '../page-objects/restaurant';

describe('Restaurant', () => {
  context('API', () => {
    it('Restaurant GET', () => {
      cy.request('GET', 'https://content.demo.microfrontends.com/restaurants/1.json')
        .as('restaurant');
      cy.get('@restaurant')
        .its('status')
        .should('equal', 200);
      cy.get('@restaurant')
        .its('body')
        .should('have.all.keys', ['id', 'description', 'imageDescription', 'imageSrc', 'menu', 'name', 'priceRange']);
      cy.get('@restaurant')
        .its('body.menu.0')
        .should('have.all.keys', ['item', 'price']);
    });
  });

  context('STUB', () => {
    beforeEach(() => {
      cy.server();
      cy.route('GET', '**/restaurants/*', 'fixture:restaurant');
      restaurantPage.navigate('1');
    });

    it('Restaurant item render', () => {
      restaurantPage.validateName('Tumanyan Shaurma');
      restaurantPage.validateDescription('Armenian fast food chain "Tumanyan Shaurma" was established in 1998.');
      restaurantPage.validateMenuItem('SHAURMA', 0);
      restaurantPage.validateMenuItem('$14', 1);
      restaurantPage.validateMenuItem('$0', 2);
      restaurantPage.validateMenuItem('x0', 2);
    });

    it('Adding item to cart', () => {
      restaurantPage.validateDisabledButton('-', 1);
      restaurantPage.addToCart(1, 3);
      restaurantPage.removeFromCart(1, 1);
      restaurantPage.addToCart(0, 2);
      restaurantPage.validateItemPrice(0);
      restaurantPage.validateItemPrice(1);
      restaurantPage.validateTotalPrice();
    });

    it('Order now', () => {
      restaurantPage.addToCart(0, 3);
      restaurantPage.removeFromCart(0, 1);
      restaurantPage.addToCart(2, 2);
      restaurantPage.validateOrder();
    });
  });

  context('E2E', () => {
    it('Make an oder in selected restaurant', () => {
      restaurantPage.navigate('3');
      restaurantPage.addToCart(0, 1);
      restaurantPage.addToCart(1, 2);
      restaurantPage.addToCart(2, 12);
      restaurantPage.removeFromCart(2, 1);
      cy.applitoolsSetup('Restaurant Page', 'Restaurant');
      restaurantPage.validateTotalPrice();
      restaurantPage.validateOrder();
    });
  });
});
