import * as restaurantsPage from '../page-objects/restaurants';
describe('Restaurants', () => {
  context('API', () => {
    it('Restaurants GET', () => {
      cy.request('GET', 'https://content.demo.microfrontends.com/restaurants.json')
        .as('restaurants');
      cy.get('@restaurants')
        .its('status')
        .should('equal', 200);
      cy.get('@restaurants')
        .its('body.0')
        .should('have.all.keys', ['id', 'description', 'imageDescription', 'imageSrc', 'menu', 'name', 'priceRange']);
    });
  });

  context('STUB', () => {
    beforeEach(() => {
      cy.server();
      cy.route('GET', '**/restaurants.json', 'fixture:restaurants');
      restaurantsPage.navigate();
    });

    it('Restaurants item render', () => {
      restaurantsPage.validateName('Tumanyan Shaurma', 0);
      restaurantsPage.validatePrice('$', 0);
      restaurantsPage.validateDescription('Armenian fast food chain "Tumanyan Shaurma" was established in 1998.', 0);
    });

    it('Filter restaurants by title', () => {
      restaurantsPage.searchFilter('sliced');
      restaurantsPage.validateName('Sliced', 0);
    });

    it('Filter restaurants by description', () => {
      restaurantsPage.searchFilter('indian');
      restaurantsPage.validateDescription('The best Indian curries from the freshest ingredients', 0);
    });

    it('Filter restaurants by price range and clear results', () => {
      restaurantsPage.priceFilter(4);
      restaurantsPage.validateName('Taste of Iberia', 0);
      restaurantsPage.priceFilter(1);
      restaurantsPage.validateRestaurantsCount(4);
      restaurantsPage.clearFilters();
      restaurantsPage.validateRestaurantsCount(9);
    });

    it('Backend error', () => {
      cy.server();
      cy.route({
        method: 'GET',
        url: '**/restaurants.json',
        status: 500,
        response: {}
      });
      cy.get('#Browse-container > div')
        .should('have.text', 'Sorry, but the restaurant list is unavailable right now');
      cy.get('[href*=restaurant]')
        .should('not.exist');
    });
  });

  context('E2E', () => {
    it('Filter restaurants and open desired one', () => {
      restaurantsPage.navigate();
      restaurantsPage.searchFilter('best');
      restaurantsPage.validateRestaurantsCount(3);
      restaurantsPage.priceFilter(2);
      restaurantsPage.validateRestaurantsCount(1);
      restaurantsPage.validateDescription('The city\'s best Southern-style fried chicken', 0);
      restaurantsPage.validateName('Crunchy Crunch', 0);
      restaurantsPage.validatePrice('$$', 0);
      cy.applitoolsSetup('Restaurants Page', 'Restaurants');
      restaurantsPage.openRestaurant('https://demo.microfrontends.com/restaurant/8', 0);
    });
  });
});
