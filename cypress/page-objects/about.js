export function navigate() {
  cy.visit('/about');
}

export function validateAboutHeader(text) {
  cy.get('h2')
    .should('have.text', text);
}

export function validateAboutText(text, lineNumber) {
  cy.get(`#about > p:nth-child(${lineNumber + 1})`)
    .should('contain', text);
}
