export function navigate(urls) {
  cy.visit('/');

  cy.get('nav')
    .should('be.visible');

  urls.forEach(url => {
    cy.contains(`${url.tabName}`)
      .click();
    cy.get(`${url.elementLocator}`)
      .should('be.visible');
  });
}

export function validateHeader() {
  cy.get('header > div')
    .should('have.text', '🍽 Feed me');
}
