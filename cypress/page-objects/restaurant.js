// / <reference types="cypress" />
export function navigate(number) {
  cy.visit('/restaurant/' + number);
}

export function validateName(restaurantName) {
  cy.get('#Restaurant-container  h1').should('have.text', restaurantName);
}

export function validateDescription(restaurantDescription) {
  cy.get('figcaption').should('have.text', restaurantDescription);
}

export function validateMenuItem(itemParameter, menuItemNumber) {
  cy.get(`section ol > li:nth-child(${menuItemNumber + 1})`)
    .contains(itemParameter)
    .should('be.visible');
}

export function validateDisabledButton(buttonText, menuItemNumber) {
  cy.get(`section ol > li:nth-child(${menuItemNumber + 1})`)
    .contains(buttonText)
    .should('be.disabled');
}

export function addToCart(menuItemNumber, numberOfItems) {
  for (let n = 0; n < numberOfItems; n += 1) {
    cy.get(`section ol > li:nth-child(${menuItemNumber + 1})`)
      .contains('+')
      .click();
  }
}

export function removeFromCart(menuItemNumber, numberOfItems) {
  for (let n = 0; n < numberOfItems; n += 1) {
    cy.get(`section ol > li:nth-child(${menuItemNumber + 1})`)
      .contains('-')
      .click();
  }
}

export function validateItemPrice(menuItemNumber) {
  cy.get(`section ol > li:nth-child(${menuItemNumber + 1})`)
    .invoke('text')
    .then((text)=>{
      var pricesStrings = text;
      var pattern = /[0-9]+/g;
      var prices = pricesStrings.match(pattern);
      var total = prices.map(Number);
      expect(total[0] * total[1]).to.equal(total[2]);
    });
}

export function validateTotalPrice() {
  cy.get('section ol > li  span:nth-child(4)')
    .invoke('text')
    .then((text)=>{
      var pricesStrings = text;
      var pattern = /[0-9]+/g;
      var prices = pricesStrings.match(pattern);
      var total = prices.map(Number).reduce(function sum(a, b) { return a + b; }, 0);
      cy.get('section div:nth-child(2)')
        .should('contain', total);
    });
}

export function validateOrder() {
  const stub = cy.stub();
  cy.on('window:alert', stub);
  cy.get('section > button').click()
    .then(() => {
      expect(stub.getCall(0)).to.be.calledWithMatch('Thank you for your order of the following delicious JSON:');
      expect(stub.getCall(0)).to.be.calledWithMatch('(This is the end of the demo)');
    });
}
