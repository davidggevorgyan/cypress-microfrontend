export function navigate() {
  cy.visit('/');
}

export function getRestaurantsList() {
  cy.get('[href*=restaurant]')
    .as('list');
}

export function validateName(name, restaurantNumber) {
  getRestaurantsList();
  cy.get('@list')
    .find('div > h2')
    .eq(restaurantNumber).should('have.text', name);
}

export function validatePrice(price, restaurantNumber) {
  getRestaurantsList();
  cy.get('@list')
    .find('div > span')
    .eq(restaurantNumber).should('have.text', price);
}

export function validateDescription(description, restaurantNumber) {
  getRestaurantsList();
  cy.get('@list')
    .find('p')
    .eq(restaurantNumber).should('have.text', description);
}

export function validateRestaurantsCount(restaurantsNumber) {
  getRestaurantsList();
  cy.get('@list')
    .should('have.length', restaurantsNumber);
}

export function searchFilter(text) {
  cy.get('[type=text]')
    .type(text);
}

export function priceFilter(priceOption) {
  cy.get(`label:nth-child(${priceOption}) > [type=checkbox]`).click();
}

export function clearFilters() {
  cy.contains('Clear').click();
}

export function openRestaurant(restaurantURL, restaurantNumber) {
  getRestaurantsList();
  cy.get('@list')
    .find('div > h2')
    .eq(restaurantNumber - 1)
    .click();
  cy.url()
    .should('eq', restaurantURL);
}
