// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
Cypress.Commands.add('applitoolsSetup', (testName, suiteName) => {
  if (Cypress.env('applitools') === true) {
    cy.eyesOpen({
      appName: 'Micro Frontend Cypress Demo',
      batchName: Cypress.env('pipeline') + ' - ' + suiteName,
      browser: [
        { name: 'chrome', width: 1920, height: 1080 },
        { name: 'firefox', width: 1920, height: 1080 },
        { name: 'ie11', width: 1920, height: 1080 },
        { deviceName: 'iPhone X' }
      ]
    });
    cy.eyesCheckWindow(testName);
    cy.eyesClose();
  }
});
